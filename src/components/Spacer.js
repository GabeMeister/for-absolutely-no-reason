import React from 'react';
import styled from 'styled-components';

const Spacer = ({ height }) => {
  const StyledSpacer = styled.div`
    padding-top: ${height}
  `;
  return <StyledSpacer />;
};

export default Spacer;
