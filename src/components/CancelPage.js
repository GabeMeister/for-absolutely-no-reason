import React from 'react';
import { Link as RouterLink } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Spacer from './Spacer';

const CancelPage = () => {
  localStorage.removeItem('success_key');

  return (
    <>
      <Spacer height="5%" />
      <Typography variant="h2">Makes sense.</Typography>
      <Spacer height="40px" />
      <Button color="primary" variant="contained" component={RouterLink} to="/">
        Back to Home
      </Button>
    </>
  );
}

export default CancelPage;
