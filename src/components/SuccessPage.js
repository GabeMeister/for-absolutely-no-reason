import React, { useEffect } from 'react';
import Link from '@material-ui/core/Link';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';

import config from '../config';
import Spacer from './Spacer';

const SuccessPage = () => {
  let successKey = new URLSearchParams(window.location.search).getAll('success_key')[0];

  if (config.CONTEXT !== 'development') {
    if (localStorage.getItem('success_key') === successKey) {
      localStorage.removeItem('success_key');
    }
    else {
      window.location.href = window.location.origin;
    }
  }

  useEffect(() => {
    fetch('.netlify/functions/twitter', {
      method: 'POST',
      body: JSON.stringify({ text: 'lol' })
    })
      .then(resp => {
        console.log(resp);
      });
  }, []);

  return (
    <>
      <Spacer height="5%" />
      <Typography variant="h2">You did it.</Typography>
      <Spacer height="30px" />
      <Typography variant="h4">You paid $1.</Typography>
      <Spacer height="20px" />
      <Box fontStyle="italic">
        <Typography variant="h5" color="textSecondary">For absolutely nothing, lol</Typography>
      </Box>
      <Spacer height="20px" />
      <Link href="https://twitter.com/ForAbsolutelyN1" target="_blank" rel="noopener noreferrer">See your transaction on Twitter</Link>
    </>
  );
}

export default SuccessPage;
