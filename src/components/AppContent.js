import styled from 'styled-components';

const AppContent = styled.div`
  // background-color: #e6ffec;
  margin: auto;
  padding: 4%;
  box-sizing: border-box;
  text-align: center;
  height: 100%;
`;

export default AppContent;
