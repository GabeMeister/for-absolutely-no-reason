import React from 'react';
import { Link as RouterLink } from 'react-router-dom';
import Typography from '@material-ui/core/Typography';
import Spacer from './Spacer';
import Button from '@material-ui/core/Button';
import styled from 'styled-components';

const StyledImg = styled.img`
  width: 200px;
  border-radius: 5px;
`;

const Wrapper = styled.div`
  width: 300px;
  margin: auto;
`;

const AboutPage = () => {
  return (
    <Wrapper>
      <Spacer height="5%" />
      <StyledImg src="/sunnies.jpg" alt="sunglasses" />
      <Spacer height="5%" />
      <Typography variant="h2">Hey, I'm <a href="https://gabejensen.now.sh/" target="_blank" rel="noopener noreferrer">Gabe</a>.</Typography>
      <Spacer height="40px" />
      <Typography variant="subtitle1" color="textSecondary">
        I literally just made this site for fun. There's literally no point, it was just simply to learn how to use the <a href="https://stripe.com/docs/api" target="_blank" rel="noopener noreferrer">Stripe API</a>. The $1 you pay literally goes straight to my personal bank account, lol.
      </Typography>
      <Spacer height="40px" />
      <Button color="primary" variant="contained" component={RouterLink} to="/">
        Back to Home
      </Button>
    </Wrapper>
  );
}

export default AboutPage;
