import React from 'react';
import Button from '@material-ui/core/Button';

const PayButton = ({ onClick }) => {
  return (
    <Button
      size="large"
      variant="contained"
      color="primary"
      onClick={onClick}
    >
      Pay
    </Button>
  );
}

export default PayButton;
