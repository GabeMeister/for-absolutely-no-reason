import React from 'react';
import Typography from '@material-ui/core/Typography';
import md5 from 'md5';
import Link from '@material-ui/core/Link';
import { Link as RouterLink } from 'react-router-dom';
import HelpOutlineIcon from '@material-ui/icons/HelpOutline';
import styled from 'styled-components';

import Spacer from './Spacer';
import PayButton from './PayButton';
import config from '../config';

const Footer = styled.div`
  position: fixed;
  bottom: 10px;
  display: flex;
  justify-content: center;
  width: 90%;
`;

const HomePage = () => {
  // We're restarting the transaction process
  localStorage.removeItem('success_key');

  const buyNothing = () => {
    let stripe = window.Stripe(config.STRIPE_PUBLISHABLE_KEY);
    let successKey = md5(`${config.STRIPE_PRODUCT_SKU}__${(new Date()).getTime()}`);

    localStorage.setItem('success_key', successKey);

    stripe.redirectToCheckout({
      items: [{ sku: config.STRIPE_PRODUCT_SKU, quantity: 1 }],
      successUrl: `${window.location.href}success?success_key=${successKey}`,
      cancelUrl: `${window.location.href}cancel`
    })
      .then(function (result) {
        if (result.error) {
          var displayError = document.getElementById('error-message');
          displayError.textContent = result.error.message;
        }
      });
  };

  return (
    <>
      <Spacer height="5%" />
      <Typography variant="h2">Pay $1.</Typography>
      <Spacer height="20px" />
      <Typography variant="h4">For absolutely nothing.</Typography>
      <Spacer height="40px" />
      <PayButton onClick={() => buyNothing()} />
      <div id="error-message"></div>
      <Footer>
        <Link
          component={RouterLink}
          underline="none"
          to="/about"
        >
          <HelpOutlineIcon style={{ fontSize: 15, marginRight: 3, transform: 'translate(0, 3px)' }} />
          <Typography variant="caption">
            About this site
          </Typography>
        </Link>
      </Footer>
    </>
  );
}

export default HomePage;
