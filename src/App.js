import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from 'react-router-dom';

// App Files
import AppContent from './components/AppContent';

// Pages
import HomePage from './components/HomePage';
import SuccessPage from './components/SuccessPage';
import CancelPage from './components/CancelPage';
import AboutPage from './components/AboutPage';

function App() {
  return (
    <>
      <Router>
        <AppContent>
          <Switch>
            <Route path="/success">
              <SuccessPage />
            </Route>
            <Route path="/cancel">
              <CancelPage />
            </Route>
            <Route path="/about">
              <AboutPage />
            </Route>
            <Route path="/">
              <HomePage />
            </Route>
          </Switch>
        </AppContent>
      </Router>
    </>
  );
}

export default App;
