import Twitter from 'twitter';
import dotenv from 'dotenv';

exports.handler = function (event, context, callback) {
  if (event.headers.host && event.headers.host.indexOf('localhost') >= 0) {
    // Local environment variables from local development
    dotenv.config({ path: '.env.local' });
  }

  const postToTwitter = () => {
    let client = new Twitter({
      consumer_key: process.env.REACT_APP_TWITTER_CONSUMER_KEY,
      consumer_secret: process.env.REACT_APP_TWITTER_CONSUMER_SECRET,
      access_token_key: process.env.REACT_APP_TWITTER_ACCESS_TOKEN_KEY,
      access_token_secret: process.env.REACT_APP_TWITTER_ACCESS_TOKEN_SECRET
    });

    const timeStr = (new Date()).toLocaleTimeString();
    const dateStr = (new Date()).toLocaleDateString();

    client.post(
      'statuses/update',
      {
        status: `On ${dateStr} at ${timeStr}, a new random user paid $1 for absolutely no reason, lol.`
      },
      function (error, tweet, response) {
        if (error) {
          console.log('ERROR OCCURRED');
          console.log(error);
        };

        console.log('tweet: ', tweet);
      }
    );
  };

  postToTwitter();

  callback(null, {
    statusCode: 200,
    body: JSON.stringify({ status: 'yup' })
  });

};
